((document)=> {
'use strict';

//Ideally you would have a unique item id to track products, but tracking by name works in this small project.
const DOM = {
    findCard: id => {return document.getElementById(id);  },
    globalFontSize: ()=> {return parseInt(window.getComputedStyle(document.getElementsByTagName("html")[0]).fontSize)},
    sideMenuItems: document.getElementsByClassName("list-item"),
    cart: document.getElementsByClassName("cart")[0], 
    cartPrice: document.getElementsByClassName("cart__total")[0],
    itemNotifier: document.getElementsByClassName("cart__num-items")[0],
    checkout: document.getElementsByClassName("checkout")[0],
    checkoutItemDisplay: document.getElementsByClassName("checkout__item-display")[0],
    checkoutHeading: document.getElementsByClassName("checkout__heading")[0],
    checkoutTotal: document.getElementsByClassName("checkout__total")[0],
    checkoutItems: document.getElementsByClassName("display-row"),
    checkoutBtn: document.getElementsByClassName("checkout__checkout-btn")[0],
    checkoutThankYou: document.getElementsByClassName("checkout__thank-you")[0],
    purchaseForm: document.getElementsByClassName("checkout__purchase-form")[0],
    purchaseBackBtn: document.getElementsByClassName("purchase-form__back-btn")[0],
    purchaseBtn: document.getElementsByClassName("purchase-form__purchase-btn")[0],
    cardDetails: document.querySelectorAll("[required]"),
    checkoutItemNames: null,
    thumbPaths: null,
    cachedThumbs: null
}

function init() {

    DOM.thumbPaths = new Map();
    DOM.checkoutItemNames = new Set();
    DOM.cachedThumbs = new Map();
    Object.freeze(DOM);

    const cards = Array.from(document.getElementsByClassName("card"));

    cards.forEach( card =>  {
        const addCartBtn = card.getElementsByClassName("btn--add-cart")[0];
        const buyBtn = card.getElementsByClassName("btn--buy")[0];

        const id = card.id;
        const sizeType = card.dataset.sizetype;
        const itemPrice = card.getElementsByClassName("card__price")[0].textContent.replace(/[^0-9.]/g,""); 
        const itemName = card.getElementsByClassName("card__name")[0].textContent.replace(/[^A-Za-z]/g, ""); //ignores punctuation

        const selectionArea = card.getElementsByClassName("card__size-selector--clothes")[0];
        const sizeDropdown = card.getElementsByClassName("size-dropdown")[0];
        const sizeTiles = card.getElementsByClassName("size-tile");
        const warning = card.getElementsByClassName("card__size-warning")[0];
        const thumbNailPath = card.getElementsByClassName("card__image")[0].src.replace(".jpg", "-thumb.jpg");
        DOM.thumbPaths.set(itemName,thumbNailPath);

        //n^2 (nested loop)
        if(selectionArea) {
            selectionArea.addEventListener("click", event => {
                removeWarningStyles(event, sizeTiles,warning)
            });
        } else if(sizeDropdown) {
            sizeDropdown.addEventListener("change", event => {
                removeWarningStyles(event,sizeDropdown,warning)
            });
        }
        
        addCartBtn.addEventListener("click", updateCart.bind(null, itemName,itemPrice,id,sizeType));
        buyBtn.addEventListener("click", addItemsOpenCheckout.bind(null, itemName,itemPrice,id,sizeType));
    });
    
    const activeProducts = new URLSearchParams(window.location.search).get("category");
    const items = DOM.sideMenuItems;
    for(let i = 0; i < items.length; i++) {
        if(items[i].textContent.toLowerCase() === activeProducts) {
            items[i].classList.replace("list-item", "list-item--active");
            break;
        }
    }

    DOM.cart.addEventListener("click", openCheckout);
    DOM.checkoutBtn.addEventListener("click", showElement.bind(null, DOM.purchaseForm));

    DOM.checkoutItemDisplay.addEventListener("click", event => {
            if(event.target.matches(".checkout__close-btn, .checkout__close-btn *")) {
                closeCheckout();
            }
        }
    );
    //DOM.checkoutItemDisplay.getElementsByClassName("checkout__close-btn")[0].addEventListener("click", openCheckout);
    DOM.checkoutItemDisplay.getElementsByClassName("checkout__close-btn")[0].addEventListener("click", openCheckout);
    loadCartItems(); 

    DOM.purchaseBackBtn.addEventListener("click", hideElement.bind(null, DOM.purchaseForm));
    DOM.purchaseBtn.addEventListener("click", () => {
        let thankYou = DOM.checkoutThankYou;
        if(cardDetailsValid()) {
        clearCheckout();
        hideElement(DOM.purchaseForm);
        hideElement(DOM.checkoutHeading);
        showElement(thankYou);
        setTimeout( (thankYou) => {
            hideElement.bind(null,thankYou);
            closeCheckout();  
        }, 2000);
        
        }

    });

    //Must load cart items before else checkoutItems won't exist.

    /*for(let i = 0; i < DOM.checkoutItems.length; i++) {
        DOM.checkoutItems[i].onclick =  (event)=> {
            if (event.target.matches(".display-row__remove-btn, .display-row__remove-btn *")) {
                removeFromCheckout(event.currentTarget)
            }; 
        }
    } */
}

function addItemsOpenCheckout(itemName,itemPrice,id,sizeType) {

    if(updateCart(itemName,itemPrice,id,sizeType) !== false) {
        openCheckout();
    }

}
function hideElement(element) {
    element.style.visibility = "hidden";
}
function showElement(element) {
    element.style.visibility = "unset";
}
function openCheckout() {
    const checkout = DOM.checkout;
    checkout.style.visibility = "unset";
}

function closeCheckout() {
    const checkout = DOM.checkout;
    checkout.style.visibility = "hidden";
}

function clearCheckout() {
    const rows = DOM.checkoutItems;
    const len = rows.length;
    for(let i = 0; i < len; i++) {
        removeFromCheckout(rows[rows.length-1]);
    }
}

function cardDetailsValid() {
    const fields = DOM.cardDetails;
    for(const field of fields) {
        if(!field.validity.valid) {
            return false;
        }
         
    }
    return true;
}

function updateCart(itemName, itemPrice, id, sizeType) {
    let card = DOM.findCard(id); 
    const qtyElement = card.getElementsByClassName("quantity__selector")[0];
    const itemQty = Number(qtyElement.options[qtyElement.selectedIndex].value);
    
    sizeType = sizeChosen(sizeType,card);

    if(sizeType === false) {
        warnSizeNotChosen(card);
        return false;
    } 

        DOM.checkoutBtn.disabled = false;
    
    // Of course you would check the item information against a database, but we aren't doing that here .
    changeCartPrice(itemPrice,itemQty);  
    changeItemNotifier(itemQty);
    saveCartItems(itemName,itemPrice,itemQty, sizeType);

    // must be added to the item name distinguish the same item of different sizes.
    const thumbNailPath = DOM.thumbPaths.get(itemName);
    if(sizeType !== "") {
        itemName += " - " + sizeType;
    }
    addToCheckout(itemName,itemPrice,itemQty,thumbNailPath);
    

    return;
}



function changeCartPrice(itemPrice,itemQty) {
    const oldCartPrice = DOM.cartPrice;
    const price = parseFloat( itemPrice*itemQty );
    let total = parseFloat(oldCartPrice.textContent.replace(/[^0-9.]/g,""));
    oldCartPrice.textContent = ("$" + (total+=price).toFixed(2)); 
    DOM.checkoutTotal.textContent = oldCartPrice.textContent;
}

function changeItemNotifier(itemQty) {
    const itemNotifier = DOM.itemNotifier;
    const itemsInCart = parseInt(itemNotifier.textContent.replace(/[D]/g,""));
    if(!itemsInCart) {
        showElement(itemNotifier);
    } else if (itemsInCart + itemQty > 99) {
        itemNotifier.style.fontSize = "1.3rem";
    }
    itemNotifier.textContent = parseInt(itemsInCart + itemQty);


}

function saveCartItems(name,price,qty,chosenSize) {

    // to distinguish items of the same name with different sizes.
    let thumbPath = DOM.thumbPaths.get(name);
    if(chosenSize !== "") {  
        name += " - " + chosenSize;  
    }


    let data = localStorage.getItem(name);
    if(data) {
        data = data.split("\t");
            // storage values are separated by tabs ("\t") in the order of price, size, quantity, and thumbnail path  2020-4-19

        let oldQty = Number(data[2]);
        qty += oldQty
    }

    localStorage.setItem(name, price + "\t" + chosenSize + "\t" + qty + "\t" + thumbPath);
}

function loadCartItems() { 
            // storage values are separated by tabs ("\t") in the order of price, size, quantity, and thumbnail path  2020-4-19
            //    ex: Trooper - XL => "99.99\tXL\t10\tdata/img/coats/coat-man-2-thumb.jpg"

    if(localStorage.length > 0) {
        DOM.checkoutBtn.disabled = false;
        const prev = Object.keys(localStorage);
        for(let i = 0; i < localStorage.length; i++) {
            let name = prev[i];
            let props = localStorage.getItem(name);
            let item = props.split("\t"); 
            let price = parseFloat(item[0]);
            let qty = Number(item[2]);
            let thumbNailPath = item[3];

            changeCartPrice(price, qty);
            changeItemNotifier(qty);
            addToCheckout(name, price, qty, thumbNailPath);
        }
        
    }

    return;

}

function addToCheckout(name, price, qty, thumbNailPath) {
    const itemArea = DOM.checkoutHeading;
    const alreadyInCheckout = DOM.checkoutItemNames;
    const cachedImgs = DOM.cachedThumbs;
    let img = cachedImgs.get(thumbNailPath);
    let inCache = true;
    let imgHTML = "";
 
    if(!img) {
        imgHTML = `<img class = "display-row__thumbnail" loading = "lazy" style="height:50px; width:50px;"src = "${thumbNailPath}"></img>`
        inCache = false;
    }
    
    if(!alreadyInCheckout.has(name)) {
        const markup = 
        ` <div class="display-row">
            ${imgHTML}
            <p class = "display-row__name">${name}</p>
            <p class = "display-row__price">$${price}</p>
            <p class = "display-row__quantity">${qty}</p>
            <span class ="display-row__remove-btn"></span>                    
        </div>`;

        itemArea.insertAdjacentHTML("afterend", markup);
        const justInserted = itemArea.nextElementSibling;

        if(inCache) {
            justInserted.insertAdjacentElement("afterbegin",img);
        }
        //will be the row's closbtn
        justInserted.lastElementChild.onclick = (event)=> {        
            if (event.target.matches(".display-row__remove-btn, .display-row__remove-btn *")) {
                removeFromCheckout(event.currentTarget.parentElement); //the row itself
            }
        }; 

        //cache image 
        
        const newImg = document.createElement("img");
        
        newImg.src = thumbNailPath;
        newImg.loading = "lazy";
        newImg.classList.add("display-row__thumbnail");
        newImg.height = 5 * DOM.globalFontSize();
        newImg.width = 5 * DOM.globalFontSize();

        cachedImgs.set(thumbNailPath,newImg)
        alreadyInCheckout.add(name); 
    } else {
        for(let i = 0; i < DOM.checkoutItems.length; i++) {

            if(DOM.checkoutItems[i].getElementsByClassName("display-row__name")[0].textContent === name) {
                let oldQty = DOM.checkoutItems[i].getElementsByClassName("display-row__quantity")[0].textContent;
                let newQty =  qty + Number(oldQty);
                DOM.checkoutItems[i].getElementsByClassName("display-row__quantity")[0].textContent = newQty;
                return;
            } 
        }
    }

}

function removeFromCheckout(itemRow) {

        const price = itemRow.getElementsByClassName("display-row__price")[0].textContent.replace(/[^0-9.]/g, "");
        const qty = itemRow.getElementsByClassName("display-row__quantity")[0].textContent;
        const name = itemRow.getElementsByClassName("display-row__name")[0].textContent;
        const alreadyInCheckout = DOM.checkoutItemNames;
        alreadyInCheckout.delete(name);
        changeCartPrice(price, -qty);
        changeItemNotifier(-qty);
        
        localStorage.removeItem(name);
        itemRow.remove(); //remove item row from checkout pop-up
        if(DOM.checkoutItems.length === 0) {
            DOM.checkoutBtn.disabled = true;
        }

}


function isLetter(c) {
    return c.toLowerCase() != c.toUpperCase(); 
    //Awesome trick I found on stack overflow. (If the language has upper/lower case, only letters will be different.)
    //Won't work on caseless languages (chinese, arabic, etc)
}

function sizeChosen(sizeType,card) {

    
    let chosenSize;
    //sizeless card
    if(sizeType.length === 0) { 
        //sizeless card
        return "";
    } else if (isLetter(sizeType)) {  
        //clothes card
        
        let chosenSize = card.getElementsByClassName("size-radio-btn");
        for(let i = 0; i < chosenSize.length; i++) {
            if(chosenSize[i].checked) {
                return chosenSize[i].value;
            }
        }
        return false;


    } else {  
        //shoe card

        //const sizeElement = document.querySelector(`div[class="card__size-selector--shoes"] select[name="sizes"]`).options;
        const sizeElement = card.getElementsByClassName("size-dropdown")[0].options;
        chosenSize = sizeElement[sizeElement.selectedIndex].value;
        return chosenSize !== "" ?  chosenSize:false
    }
}

function warnSizeNotChosen(card) {
    const sizeTiles = card.getElementsByClassName("size-tile");
    if(sizeTiles.length > 0) {

        if(!sizeTiles[0].classList.contains("size-tile--not-properly-chosen")) { //turn the clothes size-tiles orange-red
            for (let tile of sizeTiles) {

                tile.classList.toggle("size-tile--not-properly-chosen");
            }

        }
    } else {
        const sizeDropdown = card.getElementsByClassName("size-dropdown")[0];  //turn the shoes size selector orange-red
        if(!sizeDropdown.classList.contains("size-dropdown--not-properly-chosen")) {
            sizeDropdown.classList.toggle("size-dropdown--not-properly-chosen");
        }
    }

    const warning = card.getElementsByClassName("card__size-warning")[0];  // put a black overlay atop the item with a "choose a size" warning
    showElement(warning);
    warning.style.opacity = ".7";
}

function removeWarningStyles( event, selector, warning) {

    if(event.target.matches(".size-tile")) { 
        for(let tile of  selector) {
            if(tile.classList.contains("size-tile--not-properly-chosen")) {
                tile.classList.toggle("size-tile--not-properly-chosen");
                
            }
        }

    } else if(event.target.matches(".size-dropdown")) { 
        if(event.target.classList.contains("size-dropdown--not-properly-chosen")) {
            event.target.classList.toggle("size-dropdown--not-properly-chosen");
        }
    }

    hideElement(warning);
    warning.style.opacity = "0";
}






document.addEventListener("DOMContentLoaded",init);
})(document);