"use strict";

const http = require("http");
const fs =require("fs");
const url = require("url");
const products = new Map();

products.set("boots", JSON.parse(fs.readFileSync(`${__dirname}/data/products/boots.json`,"utf-8")) );
products.set("coats",  JSON.parse(fs.readFileSync(`${__dirname}/data/products/coats.json`,"utf-8")) );
products.set("gloves",  JSON.parse(fs.readFileSync(`${__dirname}/data/products/gloves.json`,"utf-8")) );
products.set("hats", JSON.parse(fs.readFileSync(`${__dirname}/data/products/hats.json`,"utf-8")) );
products.set("scarves",  JSON.parse(fs.readFileSync(`${__dirname}/data/products/scarves.json`,"utf-8")) );
let css = fs.readFileSync(`${__dirname}/css/style.css`, "utf-8");

let storeJs = fs.readFileSync(`${__dirname}/store.js`, "utf-8");
//let storeHtml = fs.readFileSync(`${__dirname}/index.html`, "utf-8"); original
//let storeHtml = fs.readFileSync(`${__dirname}/store.html`, "utf-8");
let homeHtml = fs.readFileSync(`${__dirname}/home.html`, 'utf-8');
const textHead = {"Content-type":"text/html"};
//const styleHead =  {"Content-type": "text/css"};
let itemPath = "";

const server = http.createServer( (request, response) => {


    const pathName = url.parse(request.url,true).pathname;
    const category = url.parse(request.url, true).query.category;
    console.log(pathName);

    if(pathName === "/home" || pathName === "/") {

        response.writeHead(202, textHead);
        homeHtml = homeHtml.replace(/{%style%}/g, `<style>${css}</style>`)
        
        response.end(homeHtml);
        //SERVE IMAGES
    } else if ((/\.(jpg|jpeg|png|gif)$/i).test(pathName)) {
            fs.readFile(`${__dirname}/${pathName}`, (err, data) => {
                if(err) {
                    response.writeHead(404, { 'Content-type': 'image/jpg'});
                    console.log(`The image file @ ${__dirname}/${pathName} could not be served:\n ${err.name}\n ${err.message}`);
                    response.end();
                    return;
                }
                response.writeHead(200, { 'Content-type': 'image/jpg'});
                response.end(data);
            });
    } else if ((/\.svg$/i).test(pathName)) {
        fs.readFile(`${__dirname}/${pathName}`, (err, data) => {
            if(err) {
                response.writeHead(404, { 'Content-type': 'image/svg+xml'});
                console.log(`The svg file @ ${__dirname}/${pathName} could not be served:\n ${err.name}\n ${err.message}`);
                response.end();
                return;
            }
            response.writeHead(200, {'Content-type': 'image/svg+xml'});
            response.end(data);
        });
    } else if( products.has(category) ) {
        // fs.readFile(`${__dirname}/index.html`, "utf-8",(err, data) => { original
        fs.readFile(`${__dirname}/store.html`, "utf-8",(err, data) => { // testing rewrite this with the storeHtml you loaded at the top
            let mainHtml = data;
            if(err) { 
                response.end(`Error: ${err.name}\n${err.message}`);
            }
            const type = products.get(category)[0].type;
            fs.readFile(`${__dirname}/templates/template-card--${type}.html`,"utf-8", (err, cardHtml) => {
                if(err) {
                    response.end(`Error: ${err.name}\n${err.message}`);
                }
            itemPath = category + "/"; // turn /category into category/
            const newCards =  fillCardTemplate(products.get(category),itemPath, cardHtml);

            const newMarkup = `<div class = "row--flex">${newCards}</div>`;
             mainHtml = mainHtml.replace(/{%rows%}/g,newMarkup);
             mainHtml = mainHtml.replace(/{%style%}/g,`<style>${css}</style>`)
             response.writeHead(202, textHead);
             response.end(mainHtml);
            });
        });
    } else if(pathName === "/store.js"){ 
        response.writeHead('202', {"Content-Type": 'text/javascript'});   
        response.end(storeJs);
    } else {
        response.writeHead(404, textHead);
        response.end(`We couldn't find the page you're looking for. <br><a href = "/home"><b>Home Page</b></a>`);
    }
});

server.listen(1337,`127.0.0.1`, () => {
    console.log("server listening");
    
});


function fillCardTemplate(products,pathName,markup) {
    let newCards = "";
 
       // console.log(card);
        let len = products.length;
        for(let i = 0; i < len; i++) { 
            let output = markup.replace(/{%name%}/g, products[i].name);
            output = output.replace(/{%image%}/g, pathName+products[i].image);
            output = output.replace(/{%price%}/g, "$"+products[i].price);
            output = output.replace(/{%description%}/g, products[i].description);
            output = output.replace(/{%id%}/g, products[i].id);
            newCards += output;
        }
        return newCards;
 
}